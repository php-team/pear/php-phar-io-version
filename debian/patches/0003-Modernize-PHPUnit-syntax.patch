From: =?utf-8?q?David_Pr=C3=A9vot?= <taffit@debian.org>
Date: Mon, 17 Feb 2025 19:09:25 +0100
Subject: Modernize PHPUnit syntax

---
 tests/Integration/CompliesTest.php                     |  9 +++------
 tests/Integration/VersionConstraintParserTest.php      |  7 +++----
 tests/Unit/AndVersionConstraintGroupTest.php           |  8 ++++----
 tests/Unit/AnyVersionConstraintTest.php                |  5 ++---
 tests/Unit/ExactVersionConstraintTest.php              |  7 +++----
 .../Unit/GreaterThanOrEqualToVersionConstraintTest.php |  4 ++--
 tests/Unit/OrVersionConstraintGroupTest.php            | 10 +++++-----
 tests/Unit/PreReleaseSuffixTest.php                    |  9 +++------
 .../SpecificMajorAndMinorVersionConstraintTest.php     |  4 ++--
 tests/Unit/SpecificMajorVersionConstraintTest.php      |  4 ++--
 tests/Unit/VersionTest.php                             | 18 ++++++------------
 11 files changed, 35 insertions(+), 50 deletions(-)

diff --git a/tests/Integration/CompliesTest.php b/tests/Integration/CompliesTest.php
index 93bfa73..be9f02e 100644
--- a/tests/Integration/CompliesTest.php
+++ b/tests/Integration/CompliesTest.php
@@ -9,22 +9,19 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 class CompliesTest extends TestCase {
 
-    /**
-     * @dataProvider complyingProvider
-     */
+    #[DataProvider('complyingProvider')]
     public function testCompliesWhenExcepted(string $constraint, string $version): void {
         $this->assertTrue(
             (new VersionConstraintParser())->parse($constraint)->complies(new Version($version))
         );
     }
 
-    /**
-     * @dataProvider notComplyingProvider
-     */
+    #[DataProvider('notComplyingProvider')]
     public function testNotCompliesWhenExcepted(string $constraint, string $version): void {
         $this->assertFalse(
             (new VersionConstraintParser())->parse($constraint)->complies(new Version($version))
diff --git a/tests/Integration/VersionConstraintParserTest.php b/tests/Integration/VersionConstraintParserTest.php
index 53976c7..a598c1d 100644
--- a/tests/Integration/VersionConstraintParserTest.php
+++ b/tests/Integration/VersionConstraintParserTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -16,10 +17,9 @@ use PHPUnit\Framework\TestCase;
  */
 class VersionConstraintParserTest extends TestCase {
     /**
-     * @dataProvider versionStringProvider
-     *
      * @param string $versionString
      */
+    #[DataProvider('versionStringProvider')]
     public function testReturnsExpectedConstraint($versionString, VersionConstraint $expectedConstraint): void {
         $parser = new VersionConstraintParser;
 
@@ -27,10 +27,9 @@ class VersionConstraintParserTest extends TestCase {
     }
 
     /**
-     * @dataProvider unsupportedVersionStringProvider
-     *
      * @param string $versionString
      */
+    #[DataProvider('unsupportedVersionStringProvider')]
     public function testThrowsExceptionIfVersionStringIsNotSupported($versionString): void {
         $parser = new VersionConstraintParser;
 
diff --git a/tests/Unit/AndVersionConstraintGroupTest.php b/tests/Unit/AndVersionConstraintGroupTest.php
index 2f90257..9b0910f 100644
--- a/tests/Unit/AndVersionConstraintGroupTest.php
+++ b/tests/Unit/AndVersionConstraintGroupTest.php
@@ -21,11 +21,11 @@ class AndVersionConstraintGroupTest extends TestCase {
 
         $firstConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(true));
+            ->willReturn(true);
 
         $secondConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(false));
+            ->willReturn(false);
 
         $group = new AndVersionConstraintGroup('foo', [$firstConstraint, $secondConstraint]);
 
@@ -38,11 +38,11 @@ class AndVersionConstraintGroupTest extends TestCase {
 
         $firstConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(true));
+            ->willReturn(true);
 
         $secondConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(true));
+            ->willReturn(true);
 
         $group = new AndVersionConstraintGroup('foo', [$firstConstraint, $secondConstraint]);
 
diff --git a/tests/Unit/AnyVersionConstraintTest.php b/tests/Unit/AnyVersionConstraintTest.php
index 873e1e1..20e869e 100644
--- a/tests/Unit/AnyVersionConstraintTest.php
+++ b/tests/Unit/AnyVersionConstraintTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -23,9 +24,7 @@ class AnyVersionConstraintTest extends TestCase {
         ];
     }
 
-    /**
-     * @dataProvider versionProvider
-     */
+    #[DataProvider('versionProvider')]
     public function testReturnsTrue(Version $version): void {
         $constraint = new AnyVersionConstraint;
 
diff --git a/tests/Unit/ExactVersionConstraintTest.php b/tests/Unit/ExactVersionConstraintTest.php
index 49998ca..79e392e 100644
--- a/tests/Unit/ExactVersionConstraintTest.php
+++ b/tests/Unit/ExactVersionConstraintTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -35,10 +36,9 @@ class ExactVersionConstraintTest extends TestCase {
     }
 
     /**
-     * @dataProvider compliantVersionProvider
-     *
      * @param string $constraintValue
      */
+    #[DataProvider('compliantVersionProvider')]
     public function testReturnsTrueForCompliantVersion($constraintValue, Version $version): void {
         $constraint = new ExactVersionConstraint($constraintValue);
 
@@ -46,10 +46,9 @@ class ExactVersionConstraintTest extends TestCase {
     }
 
     /**
-     * @dataProvider nonCompliantVersionProvider
-     *
      * @param string $constraintValue
      */
+    #[DataProvider('nonCompliantVersionProvider')]
     public function testReturnsFalseForNonCompliantVersion($constraintValue, Version $version): void {
         $constraint = new ExactVersionConstraint($constraintValue);
 
diff --git a/tests/Unit/GreaterThanOrEqualToVersionConstraintTest.php b/tests/Unit/GreaterThanOrEqualToVersionConstraintTest.php
index defbe8b..7eaf711 100644
--- a/tests/Unit/GreaterThanOrEqualToVersionConstraintTest.php
+++ b/tests/Unit/GreaterThanOrEqualToVersionConstraintTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -32,10 +33,9 @@ class GreaterThanOrEqualToVersionConstraintTest extends TestCase {
     }
 
     /**
-     * @dataProvider versionProvider
-     *
      * @param bool $expectedResult
      */
+    #[DataProvider('versionProvider')]
     public function testReturnsTrueForCompliantVersions(Version $constraintVersion, Version $version, $expectedResult): void {
         $constraint = new GreaterThanOrEqualToVersionConstraint('foo', $constraintVersion);
 
diff --git a/tests/Unit/OrVersionConstraintGroupTest.php b/tests/Unit/OrVersionConstraintGroupTest.php
index 8c1eb21..5bc1158 100644
--- a/tests/Unit/OrVersionConstraintGroupTest.php
+++ b/tests/Unit/OrVersionConstraintGroupTest.php
@@ -21,11 +21,11 @@ class OrVersionConstraintGroupTest extends TestCase {
 
         $firstConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(false));
+            ->willReturn(false);
 
         $secondConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(true));
+            ->willReturn(true);
 
         $group = new OrVersionConstraintGroup('foo', [$firstConstraint, $secondConstraint]);
 
@@ -38,7 +38,7 @@ class OrVersionConstraintGroupTest extends TestCase {
 
         $firstConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(true));
+            ->willReturn(true);
 
         $group = new OrVersionConstraintGroup('foo', [$firstConstraint, $secondConstraint]);
 
@@ -51,11 +51,11 @@ class OrVersionConstraintGroupTest extends TestCase {
 
         $firstConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(false));
+            ->willReturn(false);
 
         $secondConstraint->expects($this->once())
             ->method('complies')
-            ->will($this->returnValue(false));
+            ->willReturn(false);
 
         $group = new OrVersionConstraintGroup('foo', [$firstConstraint, $secondConstraint]);
 
diff --git a/tests/Unit/PreReleaseSuffixTest.php b/tests/Unit/PreReleaseSuffixTest.php
index cc20517..5e9ed6f 100644
--- a/tests/Unit/PreReleaseSuffixTest.php
+++ b/tests/Unit/PreReleaseSuffixTest.php
@@ -1,15 +1,14 @@
 <?php declare(strict_types = 1);
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
  * @covers \PharIo\Version\PreReleaseSuffix
  */
 class PreReleaseSuffixTest extends TestCase {
-    /**
-     * @dataProvider greaterThanProvider
-     */
+    #[DataProvider('greaterThanProvider')]
     public function testGreaterThanReturnsExpectedResult(
         string $leftSuffixValue,
         string $rightSuffixValue,
@@ -39,9 +38,7 @@ class PreReleaseSuffixTest extends TestCase {
         ];
     }
 
-    /**
-     * @dataProvider suffixProvider
-     */
+    #[DataProvider('suffixProvider')]
     public function testParsedValue(string $suffix): void {
         $prs = new PreReleaseSuffix($suffix);
         $this->assertEquals($suffix, $prs->asString());
diff --git a/tests/Unit/SpecificMajorAndMinorVersionConstraintTest.php b/tests/Unit/SpecificMajorAndMinorVersionConstraintTest.php
index 9dabd4f..9540714 100644
--- a/tests/Unit/SpecificMajorAndMinorVersionConstraintTest.php
+++ b/tests/Unit/SpecificMajorAndMinorVersionConstraintTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -29,12 +30,11 @@ class SpecificMajorAndMinorVersionConstraintTest extends TestCase {
     }
 
     /**
-     * @dataProvider versionProvider
-     *
      * @param int  $major
      * @param int  $minor
      * @param bool $expectedResult
      */
+    #[DataProvider('versionProvider')]
     public function testReturnsTrueForCompliantVersions($major, $minor, Version $version, $expectedResult): void {
         $constraint = new SpecificMajorAndMinorVersionConstraint('foo', $major, $minor);
 
diff --git a/tests/Unit/SpecificMajorVersionConstraintTest.php b/tests/Unit/SpecificMajorVersionConstraintTest.php
index 9fc8403..1f77c98 100644
--- a/tests/Unit/SpecificMajorVersionConstraintTest.php
+++ b/tests/Unit/SpecificMajorVersionConstraintTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -29,11 +30,10 @@ class SpecificMajorVersionConstraintTest extends TestCase {
     }
 
     /**
-     * @dataProvider versionProvider
-     *
      * @param int  $major
      * @param bool $expectedResult
      */
+    #[DataProvider('versionProvider')]
     public function testReturnsTrueForCompliantVersions($major, Version $version, $expectedResult): void {
         $constraint = new SpecificMajorVersionConstraint('foo', $major);
 
diff --git a/tests/Unit/VersionTest.php b/tests/Unit/VersionTest.php
index cbe4fbd..4806d3b 100644
--- a/tests/Unit/VersionTest.php
+++ b/tests/Unit/VersionTest.php
@@ -9,6 +9,7 @@
  */
 namespace PharIo\Version;
 
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 /**
@@ -16,8 +17,6 @@ use PHPUnit\Framework\TestCase;
  */
 class VersionTest extends TestCase {
     /**
-     * @dataProvider versionProvider
-     *
      * @param string $versionString
      * @param string $expectedMajor
      * @param string $expectedMinor
@@ -26,6 +25,7 @@ class VersionTest extends TestCase {
      * @param int    $expectedReleaseCount
      * @param string $metaData
      */
+    #[DataProvider('versionProvider')]
     public function testParsesVersionNumbers(
         $versionString,
         $expectedMajor,
@@ -68,9 +68,7 @@ class VersionTest extends TestCase {
         ];
     }
 
-    /**
-     * @dataProvider versionStringProvider
-     */
+    #[DataProvider('versionStringProvider')]
     public function testOrigionalStringReturnsExceptedVersionString(string $input): void {
         $this->assertEquals(
             (new Version($input))->getOriginalString(),
@@ -78,9 +76,7 @@ class VersionTest extends TestCase {
         );
     }
 
-    /**
-     * @dataProvider versionStringProvider
-     */
+    #[DataProvider('versionStringProvider')]
     public function testAsStringReturnsExceptedVersionString(string $input, string $excepted): void {
         $this->assertEquals(
             (new Version($input))->getVersionString(),
@@ -89,10 +85,9 @@ class VersionTest extends TestCase {
     }
 
     /**
-     * @dataProvider versionGreaterThanProvider
-     *
      * @param bool $expectedResult
      */
+    #[DataProvider('versionGreaterThanProvider')]
     public function testIsGreaterThan(Version $versionA, Version $versionB, $expectedResult): void {
         $this->assertSame($expectedResult, $versionA->isGreaterThan($versionB));
     }
@@ -115,10 +110,9 @@ class VersionTest extends TestCase {
     }
 
     /**
-     * @dataProvider invalidVersionStringProvider
-     *
      * @param string $versionString
      */
+    #[DataProvider('invalidVersionStringProvider')]
     public function testThrowsExceptionIfVersionStringDoesNotFollowSemVer($versionString): void {
         $this->expectException(InvalidVersionException::class);
         new Version($versionString);
